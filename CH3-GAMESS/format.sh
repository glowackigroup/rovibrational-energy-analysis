#!/bin/bash
file=$1
awk 'BEGIN{a=100000000; btA=1.889725989;}
/CARTESIAN COORDINATES/{a=NR; i=1;}
{if(NR==(a+1)){line[i]=$0; coord[i,1]=$2; coord[i,2]=$3; coord[i,3]=$4; i=i+1;}}
{if(NR==(a+2)){line[i]=$0; coord[i,1]=$2; coord[i,2]=$3; coord[i,3]=$4; i=i+1;}}
{if(NR==(a+3)){line[i]=$0; coord[i,1]=$2; coord[i,2]=$3; coord[i,3]=$4; i=i+1;}}
{if(NR==(a+4)){line[i]=$0; coord[i,1]=$2; coord[i,2]=$3; coord[i,3]=$4; i=i+1;}}
{if(NR==(a+5)){line[i]=$0; coord[i,1]=$2; coord[i,2]=$3; coord[i,3]=$4; i=i+1;}}
{if(NR==(a+6)){
   line6=$0; coord[i,1]=$2; coord[i,2]=$3; coord[i,3]=$4
   for(j=2;j<=5;++j){
     xdis=coord[j,1]-coord[6,1]
     ydis=coord[j,2]-coord[6,2]
     zdis=coord[j,3]-coord[6,3]
     dis=sqrt(xdis^2+ydis^2+zdis^2)
     if((dis/btA)<1.20){HCLformed[j]=1;}

     if(HCLformed[j]==1){
       print 4;
       print "COORDS & VELOCITIES";
       print line[1];
       for(kk=2;kk<=5;++kk){
         if(j!=kk){print line[kk]};
       }
     }
   }
 }
}
' $file


