#!/usr/bin/python
import sys
import os
import re
import math
from numpy import *

def NormalMode(NormalModeFile,DynamicsFile,Aidx,Bidx):
#
# This program calculates energies projected onto a molecule's normal modes
# it requires:
#   1) a text file - NormalModeFile, which 
#      contains the equilibrium geometry (in Angstroms), atomic masses, and hessian
#   2) a DynamicsFile, which has Q & P (or Q & V) as a function of time
#   
#   Aidx & Bidx are the starting and final atomic indices of the molecular fragment 
#      to be analyzed within DynamicsFile
#
#  note1: this script has so far been used to analyze VENUS output files, 
#         GAMESS *.irc output files, and CHARMM output. VENUS outputs momenta in weird units;
#         GAMESS gives velocities in bohr/fs. Whatever the case, this script requires their 
#         conversion to m/s; set the appropriate flags below to convert the 
#         VENUS Qs or GAMESS Vs to the correct units
#
#  note2: this script requires that the Qs read in at the call
#            " QPFound,Q,P=ReadFixedSpaceQP(DynFile,natoms,Aidx,Bidx) "
#         are in Angstroms; edit the subroutine to do this conversion if the units are 
#         not Angstrom (e.g., GAMESS *.irc gives Qs in Bohr) 
#
#  **to use this script with an arbitrary MD output, edit the ReadFixedSpaceQP() subroutine
#    to read the Qs and Ps/Vs in whatever format you like.
#    everything should work so long as:
#    1) the Qs are in Angstroms, and 
#    2) the Ps or Vs (whatever is read) are converted to Velocities (m/s) 
#       in an appropriate subroutine [e.g., VENUSPtoV() or GAMESSVtoVSI() ]
#

  VENUS=False
  GAMESS=False
  CHARMM=True

  if not (os.path.exists(NormalModeFile)):
   print '%s does not exist...'%(NormalModeFile) 
   sys.exit()
  else:
   print '[Molden Format]'
   print 'Reading in data from %s...\n'%(NormalModeFile)

# open the file
  refGeomFile=open(NormalModeFile,'r')
  line=refGeomFile.readline()

# get the number of atoms
  line=refGeomFile.readline()
  linelist=line.split()
  natoms=int(linelist[0]);
  print 'There are ', natoms, 'atoms'

# get the equilibrium geometry, which should be in Angstroms!
  Labels,AtomicNumbers,EqGeom=GetEqGeom(refGeomFile,natoms)

# get the atomic Masses
  Masses=GetMasses(refGeomFile,natoms)

# calculate the 3n mass vector, MassMatrix, & totalMass
  massvec3n=[]
  MassMat=zeros((natoms*3,natoms*3))
  totalMass=0.0
  for i in range(0,natoms):
    totalMass=totalMass+Masses[i]
    for j in range(0,3):
      massvec3n.append(Masses[i])
      MassMat[i*3+j,i*3+j]=1/sqrt(Masses[i])

# get the hessian
  Hessian=GetHessian(refGeomFile,natoms*3)

  refGeomFile.close()

# mass weight the hessian
  for i in range(0,natoms*3):
    for j in range(0,natoms*3):
      Hessian[i][j]=Hessian[i][j]/sqrt(massvec3n[i]*massvec3n[j])

# determine the number of vibrations & rotations
  Linear,nvibs,nrots=DetermineLinearity(EqGeom,natoms)

# Diagonalize the mass weighted Hessian
  EigenVals,EigenVecs=HessDiag(Hessian,natoms*3,nvibs)

# Translate the equilibrium geometry to the CM frame
  EqCMgeom=CalculateCMgeom(EqGeom,natoms,Masses,totalMass)

# Determine the translational and rotational eigenvectors
  MWEvecs=CalculateRotTransVecs(EqCMgeom,natoms,Masses,nrots,nvibs,Linear)

# Combine the vibrational Eigenvectors with the trans & rot eigenvectors
  ii=0
  for i in range(0,nvibs):
    for j in range(0,natoms*3):
      MWEvecs[j][i+nrots+3]=EigenVecs[j][i]
    ii=ii+1

# do an in-place gram schmidt orthonormalization on the vectors
  gramm(MWEvecs,True)
  Dtmat=matrix(MWEvecs)

# transform the vectors to Cartesian space, normalize, & get reduced masses
  CartesianDisp,redmass=Normalize(array(MassMat*Dtmat))

# error check that MWEvecs diagonalizes the Mass weighted Hessian & print the frequencies
# and the CM geometry
  ModeFrequencies=printFrequencies(Dtmat,Hessian,EqCMgeom,CartesianDisp,Labels,AtomicNumbers)

#  print CartesianDisp, redmass

# read in the fixed-space geometries & velocities on which we will do normal mode analysis
  if(os.path.exists(DynamicsFile)):
    DynFile=open(DynamicsFile,'r')
    QPFound=True
  else:
    print '%s does not exist...'%(DynamicsFile) 
    sys.exit()

  i=0

#  while i<1:
  while 1:
    i=i+1
    print "\ntimestep", i
    
#   get the sequential Q and P at each timestep
    if(CHARMM):
      QPFound,Q,P=ReadCHARMMFixedSpaceQP(DynFile,natoms,Aidx,Bidx)
    elif(GAMESS):
      QPFound,Q,P=ReadGAMESSFixedSpaceQP(DynFile,natoms,Aidx,Bidx)
    elif(VENUS):
      QPFound,Q,P=ReadVENUSFixedSpaceQP(DynFile,natoms,Aidx,Bidx)

    if not(QPFound):
      print "could not read cartesian Qs and Ps!!! aborting..."
      break

#   Translate Q to the CM frame
    DynCMQ=CalculateCMgeom(Q,natoms,Masses,totalMass)

#   make sure the P matrix is in the correct units
#   for VENUS, need to convert momentum to velocity
    if (VENUS):
      Vel,totKE=VENUSPtoV(P,Masses)
#   for GAMESS, need to convert velocity to SI units
    elif (GAMESS):
      Vel,totKE= GAMESSVtoVSI(P,Masses)
    elif (CHARMM):
      Vel,totKE= CHARMMVtoVSI(P,Masses)

#   mass weight the dynamics & equilibrium geometries
    MWDynCMQ=MassWeight(DynCMQ,Masses)
    MWEqCMgeom=MassWeight(EqCMgeom,Masses)

#   find a rotation matrix that minimizes the least squares overlap 
#   between the dynamics & reference geometry
    if(not(Linear)):
      print 'calculating rotation matrix for a nonlinear reference geometry...'  
      R=CalculateNonLinearRotationMat(MWDynCMQ,MWEqCMgeom,natoms)
    elif(natoms==2):
      print 'calculating rotation matrix for a 2 atom reference geometry...'  
      R=Calculate2AtRotationMat(MWDynCMQ,MWEqCMgeom,natoms,Linear)
    elif(natoms==3):
      print 'calculating rotation matrix for a 3 atom reference geometry...'  
      R=Calculate3AtRotationMat(MWDynCMQ,MWEqCMgeom,natoms,Linear)
    else:
      if(Linear):
        print 'the reference molecular geometry is Linear...'
      else:
        print 'the reference molecular geometry is nonlinear...'
      print 'and the molecule has ',natoms,' atoms...'
      print 'No method is available for rotation into the eckart frame...'
      break
      
    print "Finished calculation of Rotation Matrix..."

#   rotate the velocities and Qs of the dynamics geometry
    Qrot=array(matrix(DynCMQ)*matrix(R))
    Vrot=array(matrix(Vel)*matrix(R))   

#   print out the geometry in the eckart frame
    print '\nGeometry in the Eckart frame'
    printGeometry(Qrot,Labels)

#   check that the rotation satisfies the Eckart conditions
    CalculateEckartConditions(Qrot,EqCMgeom,Masses)

#   find projections of the velocities onto the normal modes
    ModeKEs=CalculateNormalModeVelocities(Vrot,CartesianDisp,massvec3n,Qrot,nrots)

#   find projections of the Qs onto the normal mode displacements
    ModePEs=CalculateNormalModePEs(Qrot,EqCMgeom,CartesianDisp,massvec3n,nvibs,nrots,redmass,ModeFrequencies)




########### function to get the eq Geometry ###########

def GetEqGeom(file,nats):
  
  print 'Reading in the equilibrium geometry...'
  geomArray=zeros((nats,3))
  labes=[]
  atnums=[]

  line=file.readline()
  for i in range(0,nats):
    line=file.readline()
#    print line
    linelist=line.split()
    labes.append(linelist[0])
    atnums.append(linelist[1])
    for j in range(2,5):
      geomArray[i][j-2]=float(linelist[j])
  
  return labes,atnums,geomArray   


########### function to get the masses ###########

def GetMasses(file,nats):
  
  print 'Reading in the masses...'
  mass=[]

  line=file.readline()
#  print line
  for i in range(0,nats):
    line=file.readline()
#    print line
    linelist=line.split()
    mass.append(float(linelist[0]))
  
  return mass   


########### function to get the Hessian ###########

def GetHessian(file,matsize):
  
  print 'Reading in the Hessian...\n'
  hessArray=zeros((matsize,matsize))

  line=file.readline()
  for i in range(0,matsize):
    line=file.readline()
#    print line
    linelist=line.split()
    for j in range(0,matsize):
      hessArray[i][j]=float(linelist[j])
  
  return hessArray  


########### function to determine if the molecule is linear ###########


def DetermineLinearity(Geom,nat):

  LinFlag=True

  for i in range(0,nat-1):
    v1=[]
    v2=[]
    for j in range(0,3):
      v1.append(Geom[i][j])
      v2.append(Geom[i+1][j])
    v3=cross(v1,v2)
    testv=linalg.norm(v3,2)
    if(testv != 0):
      LinFlag=False

  if LinFlag:
    vibs=3*nat-5
    rots=2
    print "The molecule is linear. It has ", vibs, "vibrations and ", rots, "rotations...\n"
  else:
    vibs=3*nat-6
    rots=3
    print "The molecule is not linear. It has ", vibs, "vibrations and ", rots, "rotations..." 

  return LinFlag,vibs,rots   

########### calculate the eigenvalues of the mass weighted hessian ###########

def HessDiag(hess,matsize,nvib):
  
  print 'Diagonalizing the mass-weighted Hessian...'

  numat=matsize/3
  freqWavNum=[]
  evalMap={}
  sortedevals=[]
  sortedevecs=zeros((matsize,matsize))

  amu_to_SI=4.3597482e-18/(1.66053886e-27*(5.29177249e-11)**2)
  c_in_cm=299792458.0e2

  evals,eigenvecs = linalg.eig(hess)

# assign the eigenvalues to a list for indexing
  for i in range(0,matsize):
    evalMap[i]=evals[i]

# assign the keys in evalMap to evalueList
  evalueList=evalMap.keys()
# sort evalueList according to the evalMap values so that it will hold
# the original eigenvalue numbering passed from the diagonlization
  evalueList.sort(key = evalMap.__getitem__, reverse=True)

# copy the eigenvector array & sort
  for i in range(0,matsize):
   ColumnIdx=evalueList[i]
#   print "ColumnIdx",ColumnIdx, evalMap[ColumnIdx]
   for j in range(0,matsize):
    sortedevecs[j,i]=eigenvecs[j,ColumnIdx]

  for i in range(0,matsize):
   sortedevals.append(evalMap[evalueList[i]])
   a=1/(2.0*math.pi*c_in_cm)*sqrt(amu_to_SI*abs(sortedevals[i]))
   freqWavNum.append(a)
 
  print "The ", nvib, " largest frequencies (in cm-1) are:"
  for i in range(0,nvib):
   print freqWavNum[i]

  return sortedevals,sortedevecs 
 

########### translate a geometry to the CM frame ###########

def CalculateCMgeom(geom,nat,masses,totalMass):
  
  print '\nTranslating geometry to CM frame'

  CMeqGeom=zeros((nat,3))
  cm=[0.0]*3

  for i in range(0,nat):
    for j in range(0,3):
      cm[j]=cm[j]+masses[i]*geom[i][j]/totalMass

  for i in range(0,nat):
    for j in range(0,3):
      CMeqGeom[i][j]=geom[i][j]-cm[j]

#  print CMeqGeom     
  return CMeqGeom   


##### calculate the rotational & translational eigenvectors ######

def CalculateRotTransVecs(CMgeom,nat,masses,rot,vib,Lin):

# initialize the Inertial Tensor
  Tensor=zeros((3,3))

# Evecs will contain the rot, trans, and vib motions as column eigenvecs
  Evecs=zeros((nat*3,nat*3))

# calculate the Inertial Tensor matrix

  for k in range(0,nat):
    Tensor[0][0]=Tensor[0][0]+masses[k]*((CMgeom[k][1])**2+(CMgeom[k][2])**2)
    Tensor[1][1]=Tensor[1][1]+masses[k]*((CMgeom[k][0])**2+(CMgeom[k][2])**2)
    Tensor[2][2]=Tensor[2][2]+masses[k]*((CMgeom[k][0])**2+(CMgeom[k][1])**2)
    Tensor[0][1]=Tensor[0][1]-masses[k]*( CMgeom[k][0]    * CMgeom[k][1])
    Tensor[0][2]=Tensor[0][2]-masses[k]*( CMgeom[k][0]    * CMgeom[k][2])
    Tensor[1][2]=Tensor[1][2]-masses[k]*( CMgeom[k][1]    * CMgeom[k][2])

  Tensor[1][0]=Tensor[0][1] 
  Tensor[2][0]=Tensor[0][2]
  Tensor[2][1]=Tensor[1][2]

# diagonalize the moment of Inertia Tensor
  momInertia,princAx = linalg.eig(Tensor)
  print "The moments of inertia of the equilibrium geometry are:"
  for i in range(0,rot):
    print momInertia[i]
  print " "

# using similarity transformation, find the M^(-1/2), where M is the Inertial Tensor Matrix 
  InvSqD=zeros((3,3))
  for i in range(0,3):
    if not(momInertia[i]==0.0):
      InvSqD[i][i]=1.0/sqrt(momInertia[i])

  InvSqTensor=(linalg.inv(princAx))*InvSqD*princAx

# calculate the eigenvectors for rotational motion  
  ctr=0
  for atoms in range(0,nat):
    for dimns in range(1,4): 
      for alpha in range(1,4): 
        for beta in range(1,4): 
          for j in range(0,rot):
            Evecs[ctr][j]=Evecs[ctr][j]+InvSqTensor[j][alpha-1]*LV(alpha,beta,dimns)*CMgeom[atoms][beta-1]*sqrt(masses[atoms])
      ctr=ctr+1

# calculate the eigenvectors for translational motion  
  totalMass=0.0
  for k in range(0,nat):
    totalMass=totalMass+masses[k]

  vidx=0
  for k in range(0,nat):
    for j in range(rot,rot+3):
      Evecs[vidx][j]=sqrt(masses[k]/totalMass)
      vidx=vidx+1

#  for i in range(0,rot+3):
#    for j in range(0,nat*3):
#      print Evecs[j][i]
#    print " "
  
  return Evecs


######## Gram Schmidt orthonormalization function #########
######## from http://adorio-research.org/wordpress/?p=184

def gramm(X,inplace = False):
    # Returns the Gramm-Schmidt orthogonalization of matrix X
    if not inplace:
       V = [row[:] for row in X]  # make a copy.
    else:
       V = X
    k = len(X[0])          # number of columns. 
    n = len(X)             # number of rows.
 
    for j in range(k):
       for i in range(j):
          # D = < Vi, Vj>
          D = sum([V[p][i]*V[p][j] for p in range(n)])
 
          for p in range(n): 
            # Note that the Vi's already have length one!
            # Vj = Vj - <Vi,Vj> Vi/< Vi,Vi >
            V[p][j] -= (D * V[p][i])
 
       # Normalize column V[j]
       invnorm = 1.0 / sqrt(sum([(V[p][j])**2 for p in range(n)]))
       for p in range(n):
           V[p][j] *= invnorm
    return V


########### function to calculate Levi-Civita tensor components ###########

def LV(i,j,k):
  value=(j-i)*(k-i)*(k-j)/2
  return value


######## function to normalize the column vectors of a matrix ############

def Normalize(Mat):

  k = len(Mat[0])          # number of columns. 
  n = len(Mat)             # number of rows.

  normSquared=[]
  NormMat=zeros((n,k))

  for i in range(0,k):
    invnorm = 0.0
    for j in range(0,n):
      invnorm += (Mat[j][i])**2
    normSquared.append(1/invnorm)
    for j in range(0,n):
      NormMat[j][i] = sqrt(normSquared[i])*Mat[j][i]

  return  NormMat, normSquared

######### function to normalize a vector ##########

def VecNorm(vec):

  n=linalg.norm(vec)
  normvec=[]
  for i in range(0,len(vec)):
    normvec.append(vec[i]/n)

  return normvec

######## function to read in fixed space CHARMM Ps & Qs ##########

def ReadCHARMMFixedSpaceQP(file,nat,A,B):
 
  QPretrieved=False
  Q=make_array(nat,3)
  P=make_array(nat,3)
  ctr=0
  while not QPretrieved:
    line=file.readline()
    if not line:break
    elif re.search("coordinates",line):

      for i in range(1,A):
        line=file.readline()

      for i in range(A,B+1):
        line=file.readline()
        linelist=line.split()

        for j in range(0,3):
          Q[ctr][j]=float(linelist[j+1])
          P[ctr][j]=float(linelist[j+1+3])
      
        ctr += 1
      
      QPretrieved=True

  return QPretrieved,Q,P

######## function to read in fixed space VENUS Ps & Qs ##########

def ReadVENUSFixedSpaceQP(file,nat,A,B):
 
# the Qs passed out of this subroutine should be in Angstrom
# edit the conversion factor btA if this is not the case
#  btA=1.889725989
  btA=1.0
  QPretrieved=False
  Q=make_array(nat,3)
  P=make_array(nat,3)
  ctr=0
  while not QPretrieved:
    line=file.readline()
    if not line:break
    elif re.search("XXXXXXXXXXXXXXXXXXXXXXXX TRAJECTORY NUMBER    1 ",line):

      line=file.readline()     
      line=file.readline()     
      line=file.readline()     
      line=file.readline()     
      line=file.readline()     

      for i in range(1,A):
        line=file.readline()

      for i in range(A,B+1):
        line=file.readline()
#        print line
        linelist=line.split()

        for j in range(0,3):
          Q[ctr][j]=float(linelist[j])/btA
          P[ctr][j]=float(linelist[j+3])
      
        ctr += 1
      
      QPretrieved=True

  return QPretrieved,Q,P

######## function to read in fixed GAMESS space Ps & Qs ##########

def ReadGAMESSFixedSpaceQP(file,nat,A,B):
 
  btA=1.889725989
  QPretrieved=False
  Q=make_array(nat,3)
  P=make_array(nat,3)
  ctr=0
  while not QPretrieved:
    line=file.readline()
    if not line:break
    elif re.search("COORDS & VELOCITIES",line):

      for i in range(1,A):
        line=file.readline()

      for i in range(A,B+1):
        line=file.readline()
        linelist=line.split()

        for j in range(0,3):
          Q[ctr][j]=float(linelist[j+1])/btA
          P[ctr][j]=float(linelist[j+1+3])
      
        ctr += 1
      
      QPretrieved=True

  return QPretrieved,Q,P

###### function to convert VENUS momenta to velocities #######

def VENUSPtoV(momenta,massvec):

  PtoVinSI=sqrt((4184/0.04184)*(1/(6.022e23*1.66e-27)))
  nat=len(massvec)
  V=zeros((nat,3))
  TotKEinKcal=0.0

  for i in range(0,nat):
    for j in range(0,3):
       V[i][j]=PtoVinSI*momenta[i][j]/massvec[i]
       TotKEinKcal=TotKEinKcal+(0.5*massvec[i]*1.66e-27*(V[i][j])**2)*6.022e23/1000/4.184

  return V,TotKEinKcal

###### function to convert GAMESS momenta to velocities #######

def GAMESSVtoVSI(velocity,massvec):

# convert bohr/fs to m/s
  VtoVinSI=(1.0e15)/(1.889725989e+10)
  nat=len(massvec)
  V=zeros((nat,3))
  TotKEinKcal=0.0

  for i in range(0,nat):
    for j in range(0,3):
       V[i][j]=VtoVinSI*velocity[i][j]
       TotKEinKcal=TotKEinKcal+(0.5*massvec[i]*1.66e-27*(V[i][j])**2)*6.022e23/1000/4.184

  return V,TotKEinKcal

###### function to convert CHARMM velocities to velocities in m/s #######

def CHARMMVtoVSI(momenta,massvec):

  AngPerAKMAtoMetPerSec=(1.02274E-03/5.0E-05)*1.0e12/1.0e10
  nat=len(massvec)
  V=zeros((nat,3))
  TotKEinKcal=0.0

  for i in range(0,nat):
    for j in range(0,3):
       V[i][j]=AngPerAKMAtoMetPerSec*momenta[i][j]
       TotKEinKcal=TotKEinKcal+(0.5*massvec[i]*1.66e-27*(V[i][j])**2)*6.022e23/1000/4.184

  return V,TotKEinKcal

## function to calculate the matrix that rotates Qdyn onto Qref ##
############## and satisfies the eckart conditions ###############

def Calculate2AtRotationMat(MWQdyn,MWQref,nat,Lin):

# find the normal to the plane created by the two diatomic vectors 
 
  v1=VecNorm(MWQdyn[0]-MWQdyn[1])
  v2=VecNorm(MWQref[0]-MWQref[1])

#  print "MWQref",MWQref
#  print "MWQdyn",MWQdyn
#  print "v1", v1
#  print "v2", v2

  vnorm=VecNorm(cross(v2,v1))
#  print "vnorm", vnorm

# find the angle for rotation about this unit vector & rotate

  angle=arccos(dot(v1,v2))
#  print "angle",angle

  R1=RotationMatrix(vnorm,angle)
#  print R1

  return R1

## function to calculate the matrix that rotates Qdyn onto Qref ##
############## and satisfies the eckart conditions ###############

def Calculate3AtRotationMat(MWQdyn,MWQref,nat,Lin):

# find the normal to the plane of the MWQref geometry  
  if Lin:
    vnorm1=[1.0,0.0,0.0]
    print "\nWarning: there is no molecular plane for a 3-atom linear molecule"
    print "using", vnorm1, "as the normal vector...\n"
  else:
#   find the normal to the plane of the MWQref geometry  
    v1=MWQref[0]-MWQref[1]
    v2=MWQref[2]-MWQref[1]
    vnorm1=VecNorm(cross(v1,v2))

# find the normal to the plane of the MWQdyn geometry  
  v1=MWQdyn[0]-MWQdyn[1]
  v2=MWQdyn[2]-MWQdyn[1]
  vnorm2=VecNorm(cross(v1,v2))

# find a unit vector along the seam of intersection between planes
  vintersect=VecNorm(cross(vnorm1,vnorm2))

# find the angle for rotation about this unit vector & rotate
  a=arccos(dot(vnorm1,vnorm2))
  R1=RotationMatrix(vintersect,a)
  MWQdynI=(matrix(MWQdyn))*(matrix(R1))

# find the second rotation required to minimize the least squares
  C=0.0
  S=0.0

  for i in range(0,3):
    C=C+dot(MWQdynI[i],MWQref[i])
    S=S+dot(cross(MWQdynI[i],MWQref[i]),vnorm1)

  a=sqrt(S**2+C**2)

# the rotation angle about vnorm 1 may be either theta1 or theta2
# check which gives the minimum least squares & choose that one
  theta1=-1.0*arctan(S/C)
  theta2=theta1-math.pi

  R2theta1=RotationMatrix(vnorm1,theta1)
  GeomTheta1=array(matrix(MWQdynI)*matrix(R2theta1))

  R2theta2=RotationMatrix(vnorm1,theta2)
  GeomTheta2=array(matrix(MWQdynI)*matrix(R2theta2))

  LS1=0.0
  LS2=0.0
  for i in range(0,nat):
    for j in range(0,3):
      LS1 += (GeomTheta1[i][j]-MWQref[i][j])**2
      LS2 += (GeomTheta2[i][j]-MWQref[i][j])**2
  if(LS1<LS2):
    R2=R2theta1
  else:
    R2=R2theta2  

# calculate the final rotation matrix as the product of R1 and R2
  Rmat=array(matrix(R1)*matrix(R2))

  return Rmat


## function to calculate the matrix that rotates Qdyn onto Qref ##
############## and satisfies the eckart conditions ###############
#### note: only works for a nonlinear reference geometry #########

def CalculateNonLinearRotationMat(MWQdyn,MWQref,nat):

  bar1=[0.0]*3
  bar2=[0.0]*3
  coordMat1p=zeros((3,nat))
  coordMat2p=zeros((3,nat))
  C=zeros((3,3))
  V=zeros((3,3))
  coordMat1=transpose(MWQdyn)
  coordMat2=transpose(MWQref)

  for i in range(0,3):
    for k in range(0,nat):
      bar1[i]=bar1[i]+coordMat1[i][k]/nat
      bar2[i]=bar2[i]+coordMat2[i][k]/nat

  for i in range(0,3):
    for k in range(0,nat):
      coordMat1p[i][k]=coordMat1[i][k]-bar1[i]
      coordMat2p[i][k]=coordMat2[i][k]-bar2[i]

  for k in range(0,nat):
    for i in range(0,3):
      for j in range(0,3):     
        C[i][j]=C[i][j]+coordMat2p[i][k]*coordMat1p[j][k]/nat; 
  
  U,S,Vt=linalg.svd(C)
  
  V[0][0]=1.0
  V[1][1]=1.0
  V[2][2]=linalg.det((matrix(U)*matrix(Vt)))

  Rmat=transpose((matrix(U)*matrix(V))*matrix(Vt))

  return Rmat



##### function to check that the eckart conditions are satisfied #########

def CalculateEckartConditions(Qdyn,Qref,massvec):

  vsum=[0.0]*3
  nat=len(massvec)

# check translational eckart conditions
  for i in range(0,nat):
    for j in range(0,3):
      vsum[j]=vsum[j]+massvec[i]*Qdyn[i][j]
  
  a=linalg.norm(vsum)
  print "\nTranslational eckart conditions are satisfied if the following is numerically zero"
  print a

  vsum=[0.0]*3

# check rotational eckart conditions
  for i in range (0,nat):
    v1=sqrt(massvec[i])*Qref[i]
    v2=sqrt(massvec[i])*Qdyn[i]
    v3=cross(v1,v2)
    for j in range (0,3):
      vsum[j]=vsum[j]+v3[j]

  print "\nRotational eckart conditions satisfied if the following vector is numerically zero"
  print vsum
  print " "


### function to construct a rotation matrix from an axis & angle (rad) ###

def RotationMatrix(axis,angle):

  rotm=zeros((3,3))

  ux=axis[0]
  uy=axis[1]
  uz=axis[2]
  sinth=sin(angle)
  costh=cos(angle)
  costhm=1.0-costh

# Form the components of the rotation matrix.

  rotm[0][0] = costhm * ux * ux + costh
  rotm[0][1] = costhm * ux * uy - sinth * uz
  rotm[0][2] = costhm * ux * uz + sinth * uy
  rotm[1][0] = costhm * uy * ux + sinth * uz
  rotm[1][1] = costhm * uy * uy + costh
  rotm[1][2] = costhm * uy * uz - sinth * ux
  rotm[2][0] = costhm * uz * ux - sinth * uy
  rotm[2][1] = costhm * uz * uy + sinth * ux
  rotm[2][2] = costhm * uz * uz + costh

  return rotm


####### function to mass weight a set of coordinates #######

def MassWeight(geom,massvec):

  nat=len(massvec)
  MWgeom=zeros((nat,3))

# mass weight the coordinates
  for i in range(0,nat):
    for j in range(0,3):
      MWgeom[i][j]=geom[i][j]*sqrt(massvec[i])

  return MWgeom


######## function to calculate projection of molecular KEs on the normal modes ######

def CalculateNormalModeVelocities(Vrot,CartesianDisp,massvec,Qrot,rots):

  nat=len(massvec)/3
  vt=[]
  NormalModeKEs=[]
  velinNM=zeros((nat*3,nat*3))
  totKE=0

  for i in range(0,nat):
    for j in range(0,3):
      vt.append(Vrot[i][j])

#  print CartesianDisp

  NMVelocities=dot(linalg.inv(CartesianDisp),vt)

#  print linalg.inv(CartesianDisp)
#  print " "
#  print vt
#  print " "
#  print NMVelocities

  for i in range(0,nat*3):
    for j in range(0,nat*3):
      velinNM[j][i]=NMVelocities[i]*CartesianDisp[j][i]

    KEinNM=0
    
    for k in range(0,nat*3):
      KEinNM=KEinNM+0.5*massvec[k]*((velinNM[k][i])**2)*1.66e-27*6.022e23/1000/4.184
     
#    print i, KEinNM
    NormalModeKEs.append(KEinNM)

    totKE=totKE+KEinNM
  
  transKE=0.0
  rotKE=0.0
  vibKE=0.0

# calculate and print out KE in each mode
  print '\nKinetic Energy (kcal/mol) projections:'
  print '-rotations-'
  for j in range(0,rots):
    print 'v %s: %f'%(j+1,NormalModeKEs[j])
  print '-translations-'
  for j in range(rots,rots+3):
    print 'v %s: %f'%(j+1,NormalModeKEs[j])
  print '-vibrations-'
  for j in range(rots+3,nat*3):
    print 'v %s: %f'%(j+1,NormalModeKEs[j])

  print '-totals-'
# calculate and print out total translational KE
  for j in range(rots,rots+3):
    transKE=transKE+NormalModeKEs[j]
  print 'trans %f'%(transKE)

# calculate and print out total rotational KE
  for j in range(0,rots):
    rotKE=rotKE+NormalModeKEs[j]
  print 'rot %f'%(rotKE)

# calculate and print out total vibrational KE
  for j in range(rots+3,nat*3):
    vibKE= vibKE +NormalModeKEs[j]
  print 'vib %f'%(vibKE)

  print 'trans+vib+rot %f'%(transKE+rotKE+vibKE)
  
  return NormalModeKEs;


######## function to calculate normal mode displacements and normal mode PE ##########

def CalculateNormalModePEs(Qrot,Qref,CartesianDisp,massvec3n,vibs,rots,mus,freqs):

  NormalModePEs=[]
  disp=[0.0]*vibs
  nat=len(massvec3n)/3
  diffvec=[]
  diffvectest=[0.0]*(nat*3)
  ctr=0

  for i in range(0,nat):
    for j in range(0,3):
      diffvec.append(Qrot[i][j]-Qref[i][j])

#  print "diffvec"
#  print diffvec

# generate the normal mode displacements by taking the inverse

  NMdisplacements=dot(linalg.inv(CartesianDisp),diffvec)

  print '\nNormal mode displacements:'

# calculate and print out displacements (dQ) in each mode
#  for j in range(0,rots):
#    print 'dQ in rotation (vector', j+1, ') :', NMdisplacements[j]
#  for j in range(rots,rots+3):
#    print 'dQ in translation (vector', j+1, ') :', NMdisplacements[j]
#  for j in range(rots+3,nat*3):
#    print 'dQ in vibration (vector', j+1, ') :', NMdisplacements[j]

# test to verify that we exactly recover the cartesian displacements
#  for i in range(0,nat*3):
#    for j in range(0,nat*3):
#      diffvectest[i]=diffvectest[i]+NMdisplacements[j]*CartesianDisp[i][j]    
#  print diffvectest, "\n"

# calculate the harmonic energies (kcal/mol) in each vibrational normal mode
  CinCMperSEC=29979245800.00
  AngToM=1.0e-10
  NA=6.022e23
  Jtokcal=1.0/1000/4.184
  print '\nHarmonic normal mode energies calculated from displacements:'
  print 'mode\tdQ\t\tfrequency\tmu\t\tV\t'
  for j in range(rots+3,nat*3):
#    print mus[j],freqs[j],NMdisplacements[j]
    omega=2*math.pi*CinCMperSEC*freqs[j]
    dQ=AngToM*NMdisplacements[j]
    mu=1.66053886e-27*mus[j]
    print 'V %s\t%f\t%f\t%f\t%f\t'%(j+1,NMdisplacements[j],freqs[j],mus[j],0.5*mu*((omega*dQ)**2)*NA*Jtokcal)
    


###### print out the frequencies obtained from the diagonalized MW Hessian #########

def printFrequencies(Dt,hess,geom,eigenvecs,labes,atnums):

  freq=[]
  btA=1.889725989
  nat=len(hess[1])/3
  amu_to_SI=4.3597482e-18/(1.66053886e-27*(5.29177249e-11)**2)
  c_in_cm=299792458.0e2

  DiagonalMatrix=array((transpose(Dt))*hess*Dt)

  print "[Atoms] Angs"
  for i in range(0,nat):
    print'%s\t%s\t%s\t%f\t%f\t%f\t'%(labes[i],i+1,atnums[i],geom[i][0],geom[i][1],geom[i][2])

  print "[FREQ]"
  for i in range(0,nat*3):
#---if the eigenvalue is positive, print it out as a positive frequency
    if(DiagonalMatrix[i][i] >= 0):
     freq.append(1/(2.0*math.pi*c_in_cm)*sqrt(amu_to_SI*DiagonalMatrix[i][i]))
     print '\t%f'%(freq[i])
#---if the eigenvalue is negative, print it out as a negative frequency
    else:
     freq.append(1/(2.0*math.pi*c_in_cm)*sqrt(amu_to_SI*abs(DiagonalMatrix[i][i])))
     print '\t-%f'%(freq[i])  
 
  print "[FR-COORD]"
  for i in range(0,nat):
    print'%s\t%f\t%f\t%f\t'%(labes[i],btA*geom[i][0],btA*geom[i][1],btA*geom[i][2])

  print "[FR-NORM-COORD]"
  for i in range(0,nat*3):
    ctr=0
    print "Vibration\t\t\t", i+1
    for k in range(0,nat):
      print'\t%f\t%f\t%f\t'%(eigenvecs[ctr][i], eigenvecs[ctr+1][i], eigenvecs[ctr+2][i])
      ctr=ctr+3

  print "\n"

  return freq


########function to print out geometry ##############

def printGeometry(geom,labes):

  vec1=[]
  vec2=[]

  nat=len(labes)
  for i in range(0,nat):
    print'%s\t%f\t%f\t%f'%(labes[i],geom[i][0],geom[i][1],geom[i][2])

# if one should want to print out a distance then uncomment & alter the section below

#  i=0
#  j=1
#  disij=sqrt((geom[i][0]-geom[j][0])**2+(geom[i][1]-geom[j][1])**2+(geom[i][2]-geom[j][2])**2)

#  print '\ndistance %s%s %s%s = %s'%(labes[i],i,labes[j],j,disij)

# if one should want to print out an angle, then uncomment & alter the section below

# assign atom indices, where j is the center atom

#  i=0
#  j=1
#  k=2
#  for ii in range(0,3):
#    vec1.append(geom[i][ii]-geom[j][ii])
#    vec2.append(geom[k][ii]-geom[j][ii])

#  angleijk=arccos(dot(VecNorm(vec1),VecNorm(vec2)))*(180.0/math.pi)

#  print 'angle %s%s %s%s %s%s = %s'%(labes[i],i,labes[j],j,labes[k],k,angleijk)


########### function to construct a 2d array ###########

def make_array(r,c):
  a=[]
  for i in range(r):
    a.append([])
    for ii in range(c):
      a[i].append(0)
  return a

if __name__ == "__main__":
#  print sys.argv
  NormalMode(sys.argv[1], sys.argv[2], int(sys.argv[3]), int(sys.argv[4]))

