#!/bin/bash
file=$1
awk 'BEGIN{a=100000000; b=0;}
/Geometry in the Eckart frame/{a=NR;print "4"; print "geom"}
{if(NR==(a+1))  print $0}
{if(NR==(a+2)){  
  print $0;
  print "X  0.000000    0.000000    -0.870805";
  print "X  0.000000    0.000000     0.046195";
 }
}
' $file


