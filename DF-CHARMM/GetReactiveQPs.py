#!/usr/bin/python
import sys
import os
import re
from math import *

def GRQPs(coordFileName,velFileName):
#
# This program analyzes CHARMM coord & velocity files, identifies if HCN was formed
# and if so, writes the coords & vels to a single file
#
# It may be run as follows:
# python CheckDirectories.py coordFile,velFile
#

   if (os.path.exists(coordFileName)):
    crdfile=open(coordFileName,'r')
   else:
    print "coordinate file does not exist... aborting...."
    sys.exit()

   if (os.path.exists(velFileName)):
    velfile=open(velFileName,'r')
   else:
    print "velocity file does not exist... aborting...."
    sys.exit()

   AnalyzeFile(crdfile,velfile)
   


########### function to construct a 2d array ###########

def make_array(r,c):
  a=[]
  for i in range(r):
    a.append([])
    for ii in range(c):
      a[i].append(0)
  return a



########### analyze the coordinate & velocity files #########

def AnalyzeFile(cfile,vfile):

  MaxTimeStepsToStore=40000
  HFstart=0
  nat=2
  HFformed=False
  Q=make_array(nat,3)
  Qstore=make_array(nat*MaxTimeStepsToStore,3)
  label=[0.0]*nat
  ctr=0
  linenumber=0
  HFs=1
  while HFs<MaxTimeStepsToStore:
    linestring=cfile.readline()
    if not linestring:break
    elif re.search("q 3 7",linestring):
      ctr=ctr+1
      for i in range(0,2):
        line=cfile.readline()
        linelist=line.split()
#        print i,linelist[6];
        label[i]=linelist[6];
        for j in range(0,3):
          Q[i][j]=float(linelist[j+9])
#          print i,j,Q[i][j]
      if not(HFformed):
        disHF=sqrt((Q[1][0]-Q[0][0])**2+(Q[1][1]-Q[0][1])**2+(Q[1][2]-Q[0][2])**2)
        if(disHF<0.917): 
          HFformed=True
          HFstart=ctr
      else:
        for i in range(0,2):
          for j in range(0,3):
            Qstore[linenumber][j]=Q[i][j]  
#          print linenumber, Qstore[linenumber][0],Qstore[linenumber][1],Qstore[linenumber][2]
          linenumber=linenumber+1
        HFs=HFs+1

  cfile.close()

  LastCount=ctr
  P=make_array(nat,3)
  ctr=0
  HFs=0
  LN=0
#  print "HFstart",HFstart,"LastCount",LastCount 
  while (ctr < LastCount):
    linestring=vfile.readline()
    if not linestring:break
    elif re.search("q 3 7",linestring):
      ctr=ctr+1
      if (ctr > HFstart): 
        for i in range(0,2):
          line=vfile.readline()
          linelist=line.split()
#          print i,linelist[6];
          for j in range(0,3):
            P[i][j]=float(linelist[j+9])
        if(HFformed):
          print "2"                      
          print "coordinates & velocities", ctr
          LN=HFs*nat
          if (HFformed):
            print label[0],Qstore[LN+0][0],Qstore[LN+0][1],Qstore[LN+0][2],P[0][0],P[0][1],P[0][2] 
            print label[1],Qstore[LN+1][0],Qstore[LN+1][1],Qstore[LN+1][2],P[1][0],P[1][1],P[1][2]        
          HFs=HFs+1

  vfile.close()

if __name__ == "__main__":
#  print sys.argv
  GRQPs(sys.argv[1], sys.argv[2])

